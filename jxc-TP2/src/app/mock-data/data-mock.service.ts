import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IData } from './IData';
import { DATA } from './mock-data';

@Injectable({
  providedIn: 'root'
})
export class DataMockService {

  constructor() { }

  getAll() : Observable<IData[]> {
    return of(DATA);
  }

  getById(kind:string,id:string) : Observable<IData|undefined> {
    let result = undefined;
    if(kind === "creature")
      result = DATA.find(element => (element.category === "creatureFood" || element.category === "creatureNoFood") && element.id === +id);
    else if (kind === "monster")
      result = DATA.find(element => element.category === "monster" && element.id === +id);

    return of(result)
  }

  getCreatures() : Observable<IData[]> {
    return of(DATA.filter(element => element.category === "creatureFood" || element.category === "creatureNoFood" ));
  }

  getMonsters() : Observable<IData[]> {
    return of(DATA.filter(element => element.category === "monster"));
  }
}
