import { Component, Input, OnInit } from '@angular/core';
import { IData } from '../mock-data/IData';
import { ActivatedRoute } from '@angular/router';
import { DataApiService } from '../api-data/data-api-service.service';

@Component({
  selector: 'app-creature',
  templateUrl: './creature.component.html',
  styleUrls: ['./creature.component.scss']
})
export class CreatureComponent implements OnInit {

  @Input() myCreature : IData|undefined;
  constructor(private dataApiService:DataApiService,private route: ActivatedRoute) { }

  getElementById(id:string):void{
    this.dataApiService.getById('creature',id)
    .subscribe(data => { 
        this.myCreature = data;
      },
      error => console.log('error load data'),
      () => console.log('one element loaded'));
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe( res => {
      const paramID = res.get("id");
      if(paramID !== null)
        this.getElementById(paramID);
    })  
  }

}
