import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { DataApiService } from '../api-data/data-api-service.service';
import { IData } from '../mock-data/IData';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  checkedMonsters : boolean = true;
  checkedCreaturesFood : boolean = true;
  checkedCreaturesNoFood : boolean = true;
  filteredByName : string = "";
  listEffects: string[] = ["defense up", "speed up", 'attack up'];
  filteredEffects: string[] = [];
  dataList :IData[] = [];
  filteredData : IData[] = [];

  searchGroup= new FormGroup({
    monstersGroup: new FormGroup({
      controlMonsters:  new FormControl(this.checkedMonsters),
      controlCreaturesFood: new FormControl(this.checkedCreaturesFood),
      controlCreaturesNoFood: new FormControl(this.checkedCreaturesNoFood),
    }),
    controlEffects: new FormControl(this.filteredEffects),
    controlName: new FormControl(this.filteredByName),
  })
  
  constructor(private dataApiService:DataApiService, private route: ActivatedRoute) {}
  
  displayEffect() :String {
    if (this.filteredEffects.length == 1) {
      return this.filteredEffects[this.filteredEffects.length-1];
    } else if (this.filteredEffects.length == 2) {
      return this.filteredEffects[this.filteredEffects.length-1] + " ( " + (this.filteredEffects.length - 1) + " element )";
    } else if (this.filteredEffects.length > 2) {
      return this.filteredEffects[this.filteredEffects.length-1] + " ( " + (this.filteredEffects.length - 1) + " elements )";
    }
    return "";
  }

  private _getAll():void{
    this.dataApiService.getAll()
    .subscribe(data => { 
      this.dataList = data; 
      this._updateFilter(); 
    }, 
      error => console.log('error load all data'),
      () => console.log('load all data'));
  }

  private _updateFilter(): void{
    this.filteredData = this.dataList
    this._updateListSlide();
    this._updateListEffect();
    this._updateListName();
  }

  private _updateListSlide(): void{
    this.filteredData = this.filteredData.filter(element => (element.category === "monster" && this.checkedMonsters) || (element.category === "creatureNoFood" && this.checkedCreaturesNoFood) || (element.category === "creatureFood" && this.checkedCreaturesFood));
  }

  private _updateListName(): void{
    this.filteredData = this.filteredData.filter(element => (element.name.includes(this.filteredByName)));
  }

  private _updateListEffect(): void{
    if (this.filteredEffects.length > 0) {
      this.filteredData = this.filteredData.filter(element =>{
          return (element.category !== "creatureFood") || ((element.cooking_effect !== undefined) && (this.filteredEffects.includes(element.cooking_effect)));
        });
    }
  }

  ngOnInit(): void {

    this._getAll();

    this.searchGroup.get("monstersGroup")!.get('controlMonsters')!
    .valueChanges.subscribe(res => { 
      this.checkedMonsters = res
      this._updateFilter();
    })

    this.searchGroup.get("monstersGroup")!.get('controlCreaturesFood')!
    .valueChanges.subscribe(res => { 
      this.checkedCreaturesFood = res
      this._updateFilter();
    })

    this.searchGroup.get("monstersGroup")!.get('controlCreaturesNoFood')!
    .valueChanges.subscribe(res => { 
      this.checkedCreaturesNoFood = res
      this._updateFilter();
    })

    this.searchGroup.get('controlEffects')!.valueChanges.subscribe(res => { 
      if (!this.filteredEffects.includes(res)) {
        this.filteredEffects.push(res);
      }
      this._updateFilter();   
    })

    this.searchGroup.get("controlName")!.valueChanges.subscribe(res => { 
      this.filteredByName = res
      this._updateFilter();
    })
  }
  
}
