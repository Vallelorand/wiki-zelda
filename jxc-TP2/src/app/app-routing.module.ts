import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreatureComponent } from './creature/creature.component';
import { HomeComponent } from './home/home.component';
import { ListComponent } from './list/list.component';
import { MonsterComponent } from './monster/monster.component';
import { SearchComponent } from './search/search.component';

const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'search', component: SearchComponent},
  {path: 'monsters', component: ListComponent},
  {path: 'monster/:id', component: MonsterComponent},
  {path: 'creatures', component: ListComponent},
  {path: 'creature/:id', component: CreatureComponent},
  {path: '', redirectTo:'home', pathMatch:"full"},
  {path: '**', component: HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
