import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { IData } from '../mock-data/IData';
import { IDataApiEntry } from './IDataApiEntry';

@Injectable({
  providedIn: 'root'
})
export class DataApiService {
  BASE_URL= "https://botw-compendium.herokuapp.com/api/v2";

  constructor(private http: HttpClient) {}

  getAll() : Observable<IData[]> {
    return this.http.get<IDataApiEntry>(this.BASE_URL)
      .pipe(
        map(res => {
          var noFood:IData[] = res["data"]['creatures']['non_food'];
          noFood.forEach((element) => {
            element["category"] = "creatureNoFood";
          });
          var food:IData[] = res["data"]['creatures']['food'];
          food.forEach((element) => {
            element["category"] = "creatureFood";
          });
          var monster:IData[] = res["data"]['monsters'];
          monster.forEach((element) => {
            element["category"] = "monster";
          });
          return [...food, ...noFood, ...monster];
        }),
      )
  }

  getById(kind:string,id:string) : Observable<IData|undefined> {
    return this.http.get<IDataApiEntry>(this.BASE_URL + "/entry/" + id)
    .pipe(
      map(res => {
        res["data"]["category"] = kind;
        return res["data"];
      })
    )
  }

  getCreatures() : Observable<IData[]> {
    return this.http.get<IDataApiEntry>(this.BASE_URL + "/category/creatures")
      .pipe(
        map(res => {
          var noFood:IData[] = res["data"]['non_food'];
          noFood.forEach((element) => {
              element["category"] = "creatureNoFood";
            });
          var food:IData[] = res["data"]['food'];
          food.forEach((element) => {
                element["category"] = "creatureFood";
              });
          return [...food, ...noFood];
        })
      )
  }

  getMonsters() : Observable<IData[]> {
    return this.http.get<IDataApiEntry>(this.BASE_URL + "/category/monsters")
      .pipe(
        map(res => {
          return [...res["data"]];
        }),
        map(res => {
          res.forEach(x => x["category"] = "monster")
          return res;
        })
      )
  }
}
