import { Component, Input, OnInit } from '@angular/core';
import { DataApiService } from '../api-data/data-api-service.service';
import { ActivatedRoute } from '@angular/router';
import { IData } from '../mock-data/IData';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @Input() dataList :IData[] = [];
  constructor(private dataApiService:DataApiService, private route: ActivatedRoute) {}

  getAll():void{
    this.dataApiService.getAll()
    .subscribe(data => { this.dataList = data }, 
      error => console.log('error load all data'),
      () => console.log('load all data'));
  }

  getMonsters():void{
    this.dataApiService.getMonsters()
    .subscribe(data => { this.dataList = data }, 
      error => console.log('error load all data'),
      () => console.log('monsters loaded'));
  }

  getCreatures():void{
    this.dataApiService.getCreatures()
    .subscribe(data => { this.dataList = data }, 
      error => console.log('error load all data'),
      () => console.log('creatures loaded'));
  }

  ngOnInit(): void {
    // this.getAll();
    this.route.url.subscribe(res => {
      console.log(res)
      res.forEach(path => {
        console.log(path.path)
        if (path.path === "monsters") {
          this.getMonsters();
        } else if (path.path === "creatures") {
          this.getCreatures();
        }
      });
    }) 
  }
}
