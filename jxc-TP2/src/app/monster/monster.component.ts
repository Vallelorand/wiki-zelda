import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataApiService } from '../api-data/data-api-service.service';
import { IData } from '../mock-data/IData';

@Component({
  selector: 'app-monster',
  templateUrl: './monster.component.html',
  styleUrls: ['./monster.component.scss']
})
export class MonsterComponent implements OnInit {

  @Input() myMonster : IData|undefined;
  constructor(private dataApiService:DataApiService,private route: ActivatedRoute) { }

  getElementById(id:string):void{
    this.dataApiService.getById('monster',id)
    .subscribe(data => { 
        this.myMonster = data;
      },
      error => console.log('error load data'),
      () => console.log('one monster loaded'));
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe( res => {
      const paramID = res.get("id");
      if(paramID !== null)
        this.getElementById(paramID);
    })  
  }

}
